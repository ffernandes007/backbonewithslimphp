var components = {
    "packages": [
        {
            "name": "jquery",
            "main": "jquery.min.js"
        },
        {
            "name": "backbone",
            "main": "backbone.min.js"
        },
        {
            "name": "underscore",
            "main": "underscore.min.js"
        }
    ],
    "shim": {
        "backbone": {
            "deps": [
                "underscore"
            ],
            "exports": "Backbone"
        },
        "underscore": {
            "exports": "_"
        }
    },
    "baseUrl": "/components"
};
if (typeof require !== "undefined" && require.config) {
    require.config(components);
} else {
    var require = components;
}
if (typeof exports !== "undefined" && typeof module !== "undefined") {
    module.exports = components;
}