App.UserList = Backbone.View.extend({
    el: '.page',
    initialize: function() {
        
        $.ajaxPrefilter(function(options, originalOptions, jqXHR) {
            options.url = "http://backbonewithslimphp.local/" + options.url;
        });
        
    },
    render: function() {
        var __this = this;
        var users  = new App.UsersCollection();
        
        users.fetch({
           success: function(data) {
               var loader = new App.Components.Loader();
               
               //loader.init("#components").include("DataTable");
               loader.init().include("DataTable"); // criar container de 'components', ler component e colocá-lo no container 
               
               // definir classes individuais para diferentes propriedades em tbody
               var valueClasses = new App.Components.SpecialCaseClass();
               valueClasses.push({prop:'name', class:["text-warning", "bg-primary"], strict:true})
                           .push({prop:'age',  class:["text-right", "bg-info"], strict:false})
                           .push({prop:'id',   class:["hide"]});
                   
               var propClasses = new App.Components.SpecialCaseClass();
               propClasses.push({prop:'id', class:["hide"]});
                           
                                                                              
               var table = new App.Components.DataTable();
               var html  = table.init({ model: App.User,
                                        componentId: "#table-component-classic",
                                        collection: data.models,
                                        tdPropClasses: ['active','masthead'],
                                        tdValueClasses: ['classe-bonita', 'classe-fantastica'],
                                        tdSpecialPropClasses: propClasses,
                                        tdSpecialValueClasses: valueClasses,
                                        dataAttributes: ["id"] }).render();
               
               __this.$el.html(html);
               
           }
           
        });
    }
});

