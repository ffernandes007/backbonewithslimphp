App.User = Backbone.Model.extend({
    defaults: {
        id:     0,
        name:  "",
        age:    0
    },
    url: function() {
        return '/users/' + this.id;
    },
    properties: function() {
        var props = [];
        
        for(x in this.defaults) {
            props.push(x);
        }
        
        return props;
    }
});

