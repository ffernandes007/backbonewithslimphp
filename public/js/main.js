if (typeof App === 'undefined') {
    var App = {};
}

App.Utils = {
    capitalize: function(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    },
    stripTags: function(data) {
        return data.toString().replace(/(<([^>]+)>)/ig,"");
    }
};

App.Components = {
    /**
     * Leitura de componentes.
     *  
     * @returns {App.Components.Loader.Anonym$0}
     */
    Loader: function() {
        return {
            path: "/components/",
            container: null,
            /**
             * Carregar o componente (nota: pedido síncrono)
             */
            init: function(container) {
                if(container !== undefined) {
                    this.container = container.replace("#","");
                }
                
                return this;
            },
            load: function(c) {
                return $.ajax({
                            type: "GET",
                            url: this.path + c + ".html",
                            async: false
                        }).responseText;
            },
            include: function(c) {
                if(this.container === undefined || this.container === null) {
                    this.container = "components";
                }
                
                if(document.getElementById(this.container) === null) {
                    $(document.body).append("<div id='" + this.container + "' class='hide'></div>");
                }
                
                this.exclude(c);
                $("#" + this.container).append(this.load(c));
                
                return this;
            },
            exclude: function(c) {
                if(document.getElementById(c) !== null) {
                    $("#" + c).remove();
                }
                
                return this;
            }
        }
    },
    /**
     * Estipula classes individuais para elementos de thead ou tbody.
     * 
     * @example
     * var valueClasses = new App.Components.SpecialCaseClass();
     * valueClasses.push({'value':'name', 'class':["special-name", "super-special"], 'strict':true})
     *             .push({'value':'age',  'class':["special-age",  "super-special"], 'strict':false});
     *             
     * No exemplo anterior sempre que for encontrado 'name' e 'age' o atributo class ser definida consoante o que for
     * encontrad em 'class'             
     * 
     * @returns {App.Components.SpecialCaseClass.Anonym$2}
     */
    SpecialCaseClass: function() {
        var classArray = [];
        return {
            prop: null,
            class: [],
            strict: false, /* true: os estilos vão ser apenas e só aqueles que forem definidos em 'class' */
            push: function(obj) {
                this.prop   = obj.prop;
                this.class  = obj.class;
                this.strict = obj.strict;
                
                classArray.push(obj);

                return this;
            },
            collection: function() {
                return classArray;
            },
            /**
             * Verifica se em classArray existe uma determinada propriedade
             */
            inArray: function(p) {
                var prop = p.toString();
                
                for(var i = 0; i < classArray.length; i++) {
                    if(classArray[i].prop === prop) {
                        return classArray[i];
                    }
                }
                
                return null;
            }
        }
    },
    /**
     * Componente para popular dados numa tabela.
     * 
     * @returns {App.Components.DataTable.Anonym$3}
     */
    DataTable: function() {
        return {
            model: {}, /* o modelo para ser percebido o conjunto de propriedades a popular no thead */
            componentClass: "table striped", /* as classes css do componente */
            componentId: null, /* id do componente no DOM */
            collection: null, /* a colecção de dados a ser populada */
            tdPropClasses: [], /* os estilos (classes) atribuídos às propriedades (a todos os 'td' de thead) */
            tdValueClasses: [], /* os estilos (classes) atribuídos aos valores (a todos os 'td' de tbody) */
            tdSpecialPropClasses: new App.Components.SpecialCaseClass(), /* classes individuais para determinadas propriedades */
            tdSpecialvalueClasses: new App.Components.SpecialCaseClass(), /* classes individuais para determinados valores consoante a propriedade desse valor*/
            dataAttributes: [], /* os valores que devem aparecer em atributos */
            init: function(obj) {
                this.model                 = _.isObject(obj.model) ? new obj.model() : this.model;
                this.componentClass        = obj.componentClass === undefined ? this.componentClass : obj.componentClass;
                this.componentId           = obj.componentId,
                this.collection            = obj.collection;
                this.tdPropClasses         = obj.tdPropClasses;
                this.tdValueClasses        = obj.tdValueClasses;
                this.tdSpecialPropClasses  = _.isObject(obj.tdSpecialPropClasses) ? obj.tdSpecialPropClasses : this.tdSpecialPropClasses;
                this.tdSpecialValueClasses = _.isObject(obj.tdSpecialValueClasses) ? obj.tdSpecialValueClasses : this.tdSpecialValueClasses;
                this.dataAttributes        = obj.dataAttributes === undefined ? this.dataAttributes : obj.dataAttributes;
                
                return this; /* para efeitos de chain */
            },
            render: function() {
                return _.template($(this.componentId).html(), { model: this.model,
                                                                componentClass: this.componentClass,
                                                                collection: this.collection,
                                                                tdPropClasses: this.tdPropClasses,
                                                                tdValueClasses: this.tdValueClasses,
                                                                tdSpecialPropClasses: this.tdSpecialPropClasses,
                                                                tdSpecialValueClasses: this.tdSpecialValueClasses,
                                                                dataAttributes: this.dataAttributes });
            }

        }
    }

};

App.Main = {
    
};

require(['js/routers/router.js',
         'js/models/user.js',
         'js/views/user-list.js',
         'js/collections/user-collection.js'],
         function() {
            var r1 = new App.Router();
            var m1 = new App.User();
            var v1 = new App.UserList();

            r1.on('route:home', render(v1));
         }
);

function render(v) {
    v.render();
}
