<?php
    require '../app/app.php';
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Backbone with SlimPHP</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="css/main.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        
        <script src="js/libs//jquery/jquery.min.js" type="text/javascript"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Backbone with SlimPHP</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Home</a></li>
                        <li><a href="#about">Sobre</a></li>
                        <li><a href="#contact">Contactos</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Opções<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#" id="">Opção1</a></li>
                                <li><a href="#" id="">Opção2</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!--/.navbar-collapse -->
            </div>
        </div>

        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
            <div class="container">
                Tutorial de Backbone (com SlimPHP)
            </div>
        </div>

        <div class="container">
            <h1>User Manager</h1>
            <hr>
            <div class="page"></div>
        </div> <!-- /container -->
        
        <footer class=" navbar navbar-fixed-bottom text-right" style="color: #fff; background-color: #000">
            <p class="container">&copy; Filipe Fernandes 2014</p>
        </footer>
        
        
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.js"><\/script>')</script>
        <!-- <script type="application/ecmascript" async="" src="https://raw.github.com/eligrey/canvas-toBlob.js/master/canvas-toBlob.js"></script>-->
        <!-- <script type="application/ecmascript" async="" src="https://raw.github.com/eligrey/FileSaver.js/master/FileSaver.js"></script>-->
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>-->
        
        <script src="js/libs/require/require.js" type="text/javascript"></script>
        <script src="js/libs/underscore/underscore-min.js" type="text/javascript"></script>
        <script src="js/libs/backbone/backbone-min.js" type="text/javascript"></script>
        <script src="js/libs/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/main.js"></script>
        
        <script>
            Backbone.history.start();
        </script>
    </body>
</html>
