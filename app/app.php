<?php

//require '../vendor/autoload.php';

require '../Slim/Slim.php';
\Slim\Slim::registerAutoloader();


$app = new \Slim\Slim(array(
    'debug'          => true,
    'mode'           => 'development',
    //'mode'          => 'production'
    'templates.path' => './templates'
));

$app->get('/', function() {

});

$app->map('/users/:id', function ($id) {
    echo "Hello {$id}";
})->via('GET');

$app->get('/users', function() {
    $json = file_get_contents("../app/data/data.json");

    print($json);

    // essencial para não renderizar o resto do layout
    exit();
});

$app->run();